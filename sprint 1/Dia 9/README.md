## Resumo do 9º Dia de Estágio

## Data: 29/02/2024

## Resumo:

No nono dia, estudamos sobre Cybersecurity, um campo crucial na era digital e também fizemos um curso sobre.

### 1º O que é Cybersecurity?

Cybersecurity é a prática de proteger sistemas, dispositivos, programas, redes e dados contra ataques maliciosos. Ela visa garantir a confidencialidade, integridade e disponibilidade de informações e recursos digitais. As Ameaças à cibersegurança incluem malware, ataques de hackers, engenharia social e erros humanos.

### 2º Cybersecurity e seus Pilares

Os três pilares fundamentais da Cybersecurity são Confidencialidade, Integridade e Disponibilidade. Esses pilares garantem que as informações sejam acessadas apenas por pessoas autorizadas (Confidencialidade), permaneçam precisas e completas (Integridade) e estejam sempre disponíveis quando necessário (Disponibilidade). Aprendemos como esses pilares se aplicam em diferentes cenários e como eles formam a base para uma estratégia de segurança cibernética eficaz.Também aprendemos a importância de equilibrar esses três pilares, pois focar demais em um pode comprometer os outros.

### 3º Como ser Hackeado e Perder Informações

Também vimos um webnar onde foi apresentado várias maneiras pelas quais os indivíduos podem ser hackeados e perder informações. Isso inclui práticas inseguras como o uso de senhas fracas, clicar em links suspeitos, baixar arquivos de fontes não confiáveis e não atualizar regularmente o software. Também explorou exemplos do mundo real de como essas práticas podem levar a violações de dados e maneiras de evitar essas armadilhas. Além disso, discutiu a importância da conscientização sobre segurança cibernética e como a educação contínua pode ajudar a prevenir ataques cibernéticos.

### 4º Segurança Wi-Fi em Rede Doméstica

Aprendemos sobre a importância da segurança Wi-Fi em redes domésticas. Discutimos várias medidas para proteger nossa rede, principalmente cuidados que temos que ter trabalhando em home office, como alterar a senha padrão do roteador, habilitar a criptografia WPA3 e desativar o WPS. Também foi apresentado os riscos de não proteger adequadamente nossa rede Wi-Fi, incluindo o acesso não autorizado e o roubo de dados. Além disso, discutimos a importância de manter-se atualizado sobre as últimas ameaças à segurança Wi-Fi e como a educação contínua pode ajudar a proteger nossa rede doméstica.

### 5º OWASP Top 10-2021

Foi apresentado o novo OWASP Top 10-2021, uma lista padrão da indústria para as vulnerabilidades de segurança mais críticas da web. Isso nos ajuda a entender e mitigar os riscos de segurança mais comuns. Vimos cada vulnerabilidade em detalhes, incluindo como elas podem ser exploradas por atacantes e como podemos proteger nossos sistemas contra essas ameaças. Também discutimos a importância de manter-se atualizado sobre as últimas vulnerabilidades e ameaças à segurança, pois os atacantes estão constantemente encontrando novas maneiras de explorar sistemas. Além disso, discutimos como a lista OWASP Top 10 pode ser usada como um guia para priorizar esforços de remediação e defesa.

### 6º Segurança Digital: Dicas para se Proteger no Dia a Dia

Recebemos dicas valiosas para proteger as informações no dia a dia. Isso inclui a criação de senhas fortes, a habilitação da autenticação de dois fatores sempre que possível e a verificação regular das configurações de privacidade nas contas online. Também vimos a importância de estar ciente das últimas ameaças de segurança e manter-se atualizado sobre as melhores práticas de segurança. Além disso, discutiu-se a importância de desenvolver uma mentalidade de segurança, onde a segurança é considerada em todas as decisões digitais que fazemos. Também exploramos como a falta de segurança digital pode levar a consequências graves, como roubo de identidade e perda de dados pessoais e prejuízos à Empresa por Falta de Cibersegurança.

### Próximos Passos:

- Continuar a aprofundar meus conhecimentos em Cybersecurity.

- Explorar mais sobre criptografia, firewalls e outras tecnologias de segurança.

