## Resumo do 8º Dia de Estágio

## Data: 28/02/2024

## Resumo:
No Oitavo dia, estudamos sobre Banco de dados NOSQL e também fizemos um exercício prático que está com a resolução na pasta chamada Exercicio NOSQL.

### 1º O que é NoSQL?

- NoSQL (ou “não apenas SQL”) é uma abordagem de design de banco de dados que difere das estruturas tradicionais encontradas em bancos de dados relacionais. Esses bancos de dados armazenam dados em formatos como documentos JSON, em vez de tabelas.

- A flexibilidade do esquema e a escalabilidade rápida são características marcantes dos bancos de dados NoSQL.

### 2º Diferenças entre NoSQL e SQL:

- O SQL (Structured Query Language) é usado para recuperar informações de bancos de dados relacionais.

- NoSQL oferece desempenho otimizado, flexibilidade e escalabilidade horizontal.
A escolha entre NoSQL e SQL depende do contexto e do caso de uso específico.

### 3º Exercício Prático:

Durante nosso estudo, realizamos um exercício prático para aplicar os conceitos aprendidos.
Exploramos como interagir com bancos de dados NoSQL e entender suas vantagens, Vantagens dos Bancos de Dados NoSQL:

- Alta escalabilidade: Permite lidar com grandes volumes de dados e cargas de trabalho distribuídas.

- Flexibilidade no esquema: Não requer uma estrutura rígida, facilitando a adaptação a mudanças nos dados.

- Modelos de dados variados: Documentos, colunas, grafos e chave-valor são alguns exemplos de modelos NoSQL.

###  4º Resolução dos exercícios 

- Com base na tabela usuarios:

  
   **1- Realizar uma consulta que conte o número de registros existentes.** 

   ![Imagem de resolução exercício 1](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio%201.png)

   
   **2- Realizar uma consulta para alterar o usuário com o nome "Teste Start" para "Teste Finish".**

   ![Imagem de resolução exercício 2](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio%202.png) 


   **3- Realizar uma consulta para encontrar o usuário com o nome "Bruce Wayne".**

   ![Imagem de resolução exercício 3](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio%203.png)

  
  
   **4- Realizar uma consulta para encontrar o usuário com o e-mail //"ghost_silva@fantasma.com".** 

   ![Imagem de resolução exercício 4](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio%204.png) 

  
  
   **5- Realizar uma consulta para deletar o usuário com e-mail "peterparker@marvel.com".** 

   ![Imagem de resolução exercício 5](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/exercicio%205.png) 




- Com base nos produtos listados, você deve:



   **6- Realizar uma consulta que apresente produtos com descrição vazia;**

   ![Imagem de resolução exercício 6](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio6.png)

  
  
   **7- Realizar uma consulta que apresente produtos com a categoria "games";**

   ![Imagem de resolução exercício 7](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio7.png)

  
  
   **8- Realizar uma consulta que apresente produtos com preço "0";**

   ![Imagem de resolução exercício 8](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio8.png)

  
  
   **9- Realizar uma consulta que apresente produtos com o preço maior que "100.00";**

   ![Imagem de resolução exercício 9](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio9.png)

  
  
   **10- Realizar uma consulta que apresente produtos com o preço entre "1000.00" e //"2000.00";** 

   ![Imagem de resolução exercício 10](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio10.png)

  
  
   **11- Realizar uma consulta que apresente produtos em que o nome contenha a palavra //"jogo".** 

   ![Imagem de resolução exercício 11](/sprint%201/Dia%208/Imagens%20resolução%20exercicios/Exercicio11.png)





### Próximos Passos:

- Continue explorando os diferentes tipos de bancos de dados NoSQL, como MongoDB.

- Aprofundar na modelagem de dados NoSQL e pratique mais exercícios para consolidar seu conhecimento.

- Explorar cenários reais de uso e considerar como os bancos de dados NoSQL podem melhorar a eficiência e escalabilidade das aplicações.