## Resumo da Atividade complementar em grupo do 4º Dia de Estágio

## Data: 22/02/2024

## Como um QA pode gerar qualidade nos projetos?

O QA é responsável por garantir que o produto/serviço final atenda aos requisitos e padrões de qualidade estabelecidos. Com o QA temos a possibilidade de realizar testes para o projeto garantindo a qualidade do software.

Para um QA gerar qualidade em um projeto, englobam-se vários pontos importantes, como: se os requisitos do projeto foram atendidos, confiança, identificação de defeitos, tomadas de decisão, redução de riscos, conformidades contratuais e regulatórias. Vale lembrar que gerar qualidade não é somente testar o software, mas garantir que a solicitação do cliente está de fato sendo realizada pela aplicação. 

O QA atua em todo o planejamento e desenvolvimento de um projeto, executando e garantindo a qualidade com os seguintes passos:

-	Identifica pontos de atenção que precisam ser mais detalhados;
-	Identifica possíveis cenários de testes;
-	Ajuda a escrever o que precisa ser atingido para atender os requisitos;
-	Documenta detalhadamente como será feito os testes que foram levantados em cada cenário;
-	Faz validações prévias de alguns cenários que já consigam ser testados;
-	Define os cenários e testes que poderão ser automatizados para otimizar alguns trabalhos manuais;
-	Testa todos os cenários levantados;
-	Reporta e documenta os bugs que surgirem no caminho;
-	Faz review de tudo que foi feito durante a Sprint;
-	Junto com o time, faz a retrospectiva levantando o que ainda precisa ser melhorado em questão de qualidade.

A qualidade não é responsabilidade única e exclusiva do QA, mas sim de todo time. Todos devem se sentir parte do produto e ter entendimento do processo, sendo responsáveis pela qualidade em suas atribuições. O papel do QA é assegurar que essa visão seja passada para a cultura da empresa e que os méritos dos resultados positivos alcançados por meio dessas ações sejam compartilhados por todos.

## Um QA do ramo financeiro também conseguiria atuar no ramo de varejo?

Um profissional de Garantia de Qualidade (QA) do setor financeiro poderia atuar no setor de varejo. No entanto, é importante notar que cada setor tem suas próprias peculiaridades e desafios. No setor financeiro, o QA pode estar acostumado com requisitos rigorosos de conformidade e segurança. No varejo, a experiência do usuário, a escalabilidade e a capacidade de lidar com picos de tráfego durante períodos de alta demanda podem ser de extrema importância. 

Portanto, embora as habilidades fundamentais de QA - como a capacidade de entender requisitos, projetar e executar testes, e colaborar com a equipe de desenvolvimento - sejam transferíveis entre os setores, pode haver uma curva de aprendizado ao se mudar para um novo setor. O profissional de QA pode precisar se familiarizar com as especificidades do setor de varejo e adaptar suas estratégias e técnicas de teste para atender às necessidades desse setor.

Isso pode envolver aprender sobre novas ferramentas ou tecnologias, entender as expectativas do cliente no setor de varejo, ou se adaptar a diferentes ciclos de lançamento de produtos. Em resumo, sim, um QA do setor financeiro pode atuar no setor de varejo, mas pode exigir algum tempo e esforço para se adaptar às diferenças entre os setores.

## Conclusão

Absorvendo todo esse conteúdo que foi disponibilizado para nós, não tem como discordar da grande importância do QA para uma empresa, uma equipe, seja essa área ou qual for.

Toda empresa deveria ter esse controle, pois isso é o propósito de oferecer algo a alguém, essa entrega de qualidade, seja um produto ou um serviço, seja ele físico ou não.

Aqui no nosso caso vamos nos aprofundar na qualidade em software, e é muito importante porque os clientes não querem lidar com um programa cheio de erros, não querem ficar abrindo chamados e aguardado a solução desses problemas, em muitos casos o tempo é muito precioso e caro.
E essa comunicação constante entre a equipe toda e o cliente é fundamental, pois as melhorias são constantes para ambos, para o cliente que consegue acompanhar e opinar sobre o projeto e o produto e para a equipe que está em constante evolução, aprendizado e comunicação.

Em suma o QA é muito importante no desenvolvimento do software pois ele ajuda na garantia do produto e que seja atendido todos os padrões e requisitos solicitados, ajuda também na identificação e na correção de defeitos melhorando o processo, mas não depende somente dele mas de toda uma equipe que realizam os processos diariamente como: Reuniões diárias, monitoramento, testes, planejamentos, execução, treinamentos entre outros. Desta forma conseguem atingir o objetivo final que é a entrega atenda todos os requisitos, de qualidade, confiabilidade, segurança e atinja as expectativas e as necessidades dos usuários. 


## Agradecimentos

A todos que contribuíram para a atividade em grupo, Obrigado!

## Contribuidores:

  **Grupo 1**

-  VINICIUS 
-  ANTONIO 
-  LETIERY 
-  DANIELE


