## Resumo do 4º Dia de Estágio

## Data: 22/02/2024

## Resumo:

No Quarto dia, estudamos sobre os fundamentos do teste de software e também tivemos uma atividade complementar em grupo sobre como um QA pode gerar qualidade nos projetos.

### 1º Fundamentos do Teste
 
### Como gerar Qualidade em um produto?

Para um QA gerar qualidade em um projeto se engloba vários pontos importantes como: se os requisitos do projeto foram atendidos, confiança, identificar defeitos, tomadas de decisão, reduzir riscos, conformidade contratuais e regulatórias.

### O que é testar?

Testar é o processo é o processo que consiste em todas as atividades de ciclos de vida estático  e dinâmico, relacionado ao planejamento preparação e avaliação de produtos de software e produtos de trabalho relacionados para determinar se satisfazem os requisitos especificados, demontrando que são adequados a sua finalidade e para detectar defeito.

### 2º teste estático e  dinâmico

### teste estático

Revisão, inspeção e análise estática dos artefatos, qualquer documento do projeto pode ser avaliado desta forma.

### teste dinâmico

Necessita que o software seja execultado, é um dos mais utilizados pelo mercado, porém o custo é mais caro.

### 3º Teste, depuração e teste de confirmação

### Teste

Teste é o processo de verificar se o software funciona como esperado. Isso envolve a execução do software sob condições controladas e a verificação dos resultados. O objetivo é encontrar e corrigir erros antes que o software seja lançado.

### Depuração

Depuração é o processo de identificar e corrigir erros no código. Quando um teste revela um problema, os desenvolvedores usam ferramentas e técnicas de depuração para encontrar a causa raiz do problema e corrigi-lo.

### Teste de Confirmação

Teste de confirmação verifica se as correções resolveram os defeitos.

### 4º Erro, defeito e Falha

### Erro

É uma ação humana que produz um resultado incorreto, pode ser cometido em qualquer fase do projeto de desenvolvimento de software.

### Defeito 

Uma imperfeição ou deficiência em um produto de trabalho que faz com que ele não atenda seus requisitos ou especificações, é o resultado do erro cometido.

### Falha

Um evento no qual o componente ou sistema não execulta uma função necessária dentro dos limites especificados.

### 5º Os sete príncipios do teste

1. O teste mostra a presença de defeitos
2. Testes exaustivos são impossíveis
3. O teste inicial economiza tempo e dinheiro
4. Defeitos se agrupam
5. Cuidado com o paradoxo do pesticida
6. O teste depende do contexto
7. Ausência de erros é uma ilusão

### 6º Atividades e Tarefas do teste

1. **Planejamento do Teste:** É como um mapa do tesouro. Aqui decidimos o que vamos testar e como vamos testar.

2. **Análise do Teste:** É como entender o quebra-cabeça. Analisamos os requisitos do software para entender o que ele deve fazer.

3. **Modelagem do Teste:** Aqui criamos “cenários” ou “casos” de teste. É como escrever uma lista de tarefas para o software.

4. **Implementação do Teste:** Preparamos o ambiente de teste e configuramos os dados necessários.

5. **Execução do Teste:** Rodamos os cenários de teste e observamos o comportamento do software. É como assistir a peça e anotar se algo deu errado.

6. **Conclusão do Teste:** Revisamos os resultados, reportamos os problemas encontrados e decidimos se o software está pronto para ser entregue.

### 7º A psicologia do teste

A psicologia em ambientes de teste é crucial. É importante manter uma mente aberta, pronta para encontrar erros e problemas, pois o objetivo é melhorar o software, não provar que ele está perfeito. Testar software pode ser um processo demorado e repetitivo, então a paciência é uma virtude necessária. A comunicação clara é fundamental para transmitir os problemas encontrados à equipe de desenvolvimento. E, por fim, a atenção aos detalhes é essencial, pois pequenos detalhes podem fazer uma grande diferença na qualidade do software. Portanto, a postura em ambientes de teste deve ser de abertura, paciência, clareza na comunicação e atenção aos detalhes.

### Próximos Passos:

- Aplicação dos Princípios do Teste: Identificar como os sete princípios do teste podem ser aplicados aos processos de teste da equipe, visando melhorar a eficácia e a qualidade do teste;

- Planejamento e Execução de Testes: Desenvolver um plano detalhado para os testes, definindo o que será testado, como será testado e quem será responsável, garantindo uma abordagem organizada e abrangente;

- Aprimoramento da Comunicação e Colaboração: Fortalecer a comunicação e a colaboração dentro da equipe de teste e com a equipe de desenvolvimento, mantendo uma mentalidade aberta, clara e colaborativa ao identificar e resolver problemas durante o processo de teste;
