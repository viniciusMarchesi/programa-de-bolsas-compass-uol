## Resumo do 3º Dia de Estágio

## Data: 21/02/2024

## Resumo:

No terceiro dia, estudamos um pouco mais sobre o scrum e sobre os papeis e responsabilidades.

### 1º Time de Desenvolvimento

É a equipe que trabalha para o desenvolvimento do produto ou projeto. A ideia principal é que a equipe de desenvolvimento seja auto gerenciável para determinar a melhor forma de atingir a meta da sprint. Lembrando que não são apenas os desenvolvedores, mas todo mundo que trabalha para a entrega do objetivo da sprint. As principais responsabilidades do time de desenvolvimento são: estimar os itens do backlog do produto, realizar as atividades para gerar um incremento no produto, gerenciar/atualizar o backlog da sprint e criar uma definição de "pronto" em acordo com o Product Owner.

### 2º Product Owner

O Product Owner pode ser alguém da área de negócios, mas o importante é que ele represente os clientes, usuários e o produto no qual estão produzindo. A ideia é de que exista um PO para cada backlog do produto. Suas principais responsabilidades são: estabelecer a visão e maximizar o ROI do produto, garantir que o backlog do produto seja transparente, visível e compreensível, fornecer feedback na revisão da sprint, fazer o planejamento das releases e decidir quando liberar o incremento em produção.

### 3º Scrum Master

O Scrum Master é o líder servidor e também um facilitador do framework Scrum para todo o restante do time e da organização. Suas principais responsabilidades são: é responsável pela eficácia do time Scrum e fazem isso permitindo que o time Scrum melhore as suas práticas dentro do framework Scrum, atender o time Scrum treinando e ajudando a focar na criação de incremento de alto valor, por exemplo, atender ao dono do produto ajudando a encontrar técnicas para definição de metas do produto, por exemplo, e atender à organização como planejar e aconselhar implementações de Scrum dentro da organização.

### 4º QA dentro de um time Ágil

O seu principal objetivo é garantir que o produto final seja entregue com a qualidade esperada o QA integra o time de desenvolvimento, porém, sendo um profissional com maior habilidade em planejar, desenvolver e executar testes, é importânte ressaltar que em times Scrum todos são multidisciplinares. Assim, todos devem executar as boas práticas de qualidade, tornando o QA em um disseminador da cultura de qualidade em software.

### Próximos Passos:

- Continuar aprofundando conhecimentos em Scrum, explorando materiais de leitura adicionais;

- Aplicar os princípios e práticas do Scrum em projetos reais, buscando oportunidades para aprimorar as habilidades de colaboração e entrega de valor.