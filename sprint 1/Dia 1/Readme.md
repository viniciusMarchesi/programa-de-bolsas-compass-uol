## Resumo do 1º Dia de Estágio

## Data: 19/02/2024

## Resumo:

No meu primeiro dia de estágio, fui introduzido a alguns conceitos fundamentais relacionados ao desenvolvimento de software e colaboração em equipe. Abaixo, detalho os principais pontos e conhecimentos aprendidos durante o dia.

## Conteúdos Abordados:

### 1. Matriz de Eisenhower

Aprendi sobre a matriz de Eisenhower, uma ferramenta de priorização de tarefas que classifica atividades com base na urgência e importância. Ela organiza as tarefas em quatro quadrantes:

1- Urgente e Importante
2- Importante, mas não Urgente
3- Urgente, mas não Importante
4- Não Urgente e Não Importante

Compreendi támbem a importância de categorizar tarefas como "urgentes e importantes", "importantes, mas não urgentes", "urgentes, mas não importantes" e "nem urgentes, nem importantes".

Essa matriz ajuda a focar nas tarefas mais críticas e a evitar distrações, promovendo uma melhor gestão do tempo e aumento da produtividade. Também foi sugerida a utilização do Miro para gerenciar as tarefas.

### 2. Git e GitHub

Recebi uma introdução ao Git, um sistema de controle de versão distribuído amplamente utilizado para rastrear alterações em arquivos, também tive a oportunidade de aprender os conceitos básicos do Git, incluindo repositórios, commits, branches e merges.

Repositórios: Locais onde os arquivos do projeto são armazenados.

Commits: alterações feitas nos arquivos do projeto, e registradas no histórico.

Branches: versões paralelas do código que permitem o desenvolvimento isolado de funcionalidades.

Merges: Integração de alterações de um branch para outro, combinando o código de forma harmônica.

Pratiquei comandos básicos do Git, como git init, git add, git commit, git push e git pull, utilizando o GitHub para hospedar um repositório de exemplo.

"git init": Inicia um novo repositório Git no diretório atual do projeto.
"git add": Prepara as mudanças nos arquivos para serem incluídas no próximo commit.
"git commit": Registra as alterações preparadas como uma nova versão no histórico do projeto.
"git push": Envia os commits locais para o repositório remoto, atualizando-o.
"git pull": Atualiza o repositório local com as alterações do repositório remoto, integrando-as ao código atual.

Aprendi sobre o GitHub, uma plataforma de hospedagem de código que permite colaboração entre desenvolvedores.

### 3 GitLab

Além do GitHub, também aprendi sobre o GitLab, outra plataforma de hospedagem de código que oferece funcionalidades semelhantes às do GitHub. E as diferenças e semelhanças entre o GitLab e o GitHub, incluindo recursos exclusivos oferecidos pelo GitLab, como integração contínua e entrega contínua (CI/CD).

### 4 Criação de README

Finalmente, aprendi sobre a importância de um bom arquivo README em projetos de software, e as melhores práticas para escrever um README claro e informativo, incluindo seções com uma descrição do projeto, instruções de instalação, uso e contribuição, entre outros....

### Próximos Passos:

- Continuar a prática e aprofundamento dos conceitos aprendidos, especialmente no uso de Git e em técnicas de colaboração em equipe;

- Explorar mais a fundo as funcionalidades avançadas do GitLab e GitHub, como gerenciamento de problemas(issues), pull requests e revisões de código;

- Aplicar os princípios da matriz de Eisenhower na organização e priorização das minhas tarefas diárias.