## Resumo do 2º Dia de Estágio

## Data: 20/02/2024

## Resumo: 

No meu segundo dia de estágio, aprofundei meus conhecimentos em metodologias ágeis, especialmente o framework Scrum, utilizado para gerenciamento de projetos de desenvolvimento de software. Abaixo, destaco os principais pontos discutidos e aprendidos durante o dia.

## Conteúdos Abordados:

### 1. Scrum

Recebi uma introdução abrangente ao Scrum, um framework ágil amplamente utilizado para gerenciamento de projetos. Compreendi os três papéis principais do Scrum: Product Owner, Scrum Master e Time de Desenvolvimento, e suas responsabilidades. O Product Owner é responsável por maximizar o valor do produto e gerenciar o Product Backlog. O Scrum Master facilita o Scrum para a equipe e remove obstáculos que possam impedir a equipe de alcançar seus objetivos. O Time de Desenvolvimento é responsável por entregar incrementos de “Pronto” ao final de cada Sprint.

Explorei os artefatos do Scrum, incluindo Product Backlog, Sprint Backlog e Incremento. O Product Backlog é uma lista ordenada de tudo o que é necessário no produto, enquanto o Sprint Backlog é um plano para a próxima Sprint que contém itens do Product Backlog. O Incremento é a soma de todos os itens do Product Backlog concluídos durante uma Sprint e todos os incrementos das Sprints anteriores.

Aprendi também sobre os eventos do Scrum, como Reunião de Planejamento da Sprint, Daily Scrum, Revisão da Sprint e Retrospectiva da Sprint. Esses eventos são essenciais para a transparência e colaboração no time, permitindo inspeção e adaptação contínuas.

### 2. Práticas Ágeis

Além do Scrum, aprendi outras práticas ágeis comumente utilizadas, como Kanban. E também a importância dos valores ágeis, como transparência, colaboração, adaptação e entrega contínua, para o sucesso de iniciativas ágeis.

Nesse contexto, também fui apresentado à ferramenta Miro, uma plataforma colaborativa online que permite a criação de quadros interativos. Miro é frequentemente usado em equipes ágeis para facilitar a colaboração remota. Ele suporta uma variedade de atividades, como brainstorming, planejamento de sprint, mapeamento de histórias de usuários e retrospecções. Com o Miro, as equipes podem visualizar seu trabalho, compartilhar ideias e colaborar em tempo real, independentemente de onde estejam localizadas.

A ferramenta Miro se mostrou útil para a implementação de práticas ágeis em um ambiente de trabalho remoto. Ela permite que as equipes mantenham a colaboração e a comunicação eficazes, que são fundamentais para a metodologia ágil, mesmo quando não estão fisicamente no mesmo local. Além disso, a capacidade de visualizar o trabalho em um formato fácil de entender ajuda as equipes a manterem-se alinhadas em seus objetivos e a entenderem melhor o progresso do projeto.

Portanto, a adição da ferramenta Miro me equipa com uma poderosa ferramenta de colaboração que pode nos ajudar a implementar efetivamente os princípios ágeis em projetos.


### Próximos Passos:

- Continuar aprofundando meus conhecimentos em metodologias ágeis, especialmente Scrum, através de leituras adicionais e prática contínua;

- Aplicar os princípios e práticas aprendidas em projetos reais, contribuindo para o sucesso da equipe e do projeto.