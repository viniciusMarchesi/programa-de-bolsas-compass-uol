## Resumo do 5º Dia de Estágio

## Data: 23/02/2024

## Resumo:

No Quinto dia, estudamos sobre os fundamentos do teste de software no backend. Foi apresentado um vídeo que abordou vários aspectos importantes do teste de software, incluindo a Pirâmide de Testes, que é um modelo usado para organizar os testes de software.

### 1º Fundamentos do Teste de Software

O vídeo apresentado abordou os fundamentos do teste de software, incluindo a Pirâmide de Testes. Esta é um modelo que ajuda a organizar os esforços de teste, com testes unitários na base, testes de integração no meio e testes end-to-end (e2e) no topo.

### 2º Ciclo de Vida do Software

O vídeo abordou o ciclo do projeto, desde o planejamento até a avaliação dos testes, destacando a importância de cada tipo de teste e fornecendo exemplos práticos de como esses testes são realizados.


### 3º Tipos de Testes

Foram destacados três tipos principais de testes: testes unitários, testes de integração e testes e2e. Os testes unitários verificam partes individuais do código, os testes de integração verificam como diferentes partes do sistema funcionam juntas e os testes e2e verificam o sistema como um todo, do início ao fim.

### 4º Exemplos Práticos

Foi demonstrado um exemplo prático de teste e2e utilizando o site da Riachuelo, desde o cadastro até a finalização da compra. Além disso, o vídeo também abordou como testar uma API, usando a API do CEP como exemplo.

### Próximos Passos:

- Continuar aprofundando conhecimentos em teste de software, explorando materiais de leitura adicionais;

- Aplicar os princípios e práticas do teste de software em projetos reais, buscando oportunidades para aprimorar as habilidades de colaboração e entrega de valor.