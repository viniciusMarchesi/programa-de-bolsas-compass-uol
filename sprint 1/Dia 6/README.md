## Resumo do 6º Dia de Estágio

## Data: 26/02/2024

## Resumo:

No Sexto dia, estudamos sobre Myers e o princípio de Pareto e também vimos um pouco mais afundo os tipos de teste.

### 1º Myers e o princípio de Pareto

Myers definiu que "testar é analisar um programa com a intenção de descobrir erros e defeitos", a regra 10 de Myers define a relação de custo com o desenvolvimento do projeto com isso, podemos observar que o quanto antes conseguirmos antecipar os testes teremos benficios tanto financeiros quanto em qualidade para o projeto, tamém podemos notar que se deixarmos para fazer os testes na fase de produção por exemplo, teremos um custo muito elevado e dai surge a importâcia de o quanto antes começarmos com os testes junto ao desenvolvimento.

o custo da correção de um defeito tende a aumentar quanto mais tarde ele for encontrado. Os defeitos encontrados nas fases iniciais do projeto de desenvolvimento do software são mais baratos do que aqueles encontrados na produção. Um estudo realizado observou que, quanto mais tempo levarmos para corrigir uma falha, mais custosa ela será. O gráfico apresentado tem como objetivo mostrar a proporção do aumento, onde, ainda na fase de levantamento de requisitos, o custo para correção é considerado muito baixo, praticamente zero. Na fase dedesenvolvimento do sistema, o custo aumenta em 10 vezes comparado com a fase anterior. Já na fase de teste unitário e teste de integração, o custo é de 100 vezes o custo inicial. Enquanto que na fase do teste de sistema, o custo será de mil (1000) vezes. O teste de aceitação custará 10 mil e quando o software está em produção, estima-se que o custo para correção de uma falha é de
100 mil vezes o custo, se a mesma fosse ajustada na fase de levantamento de requisitos.

### Princípio de Pareto

O Princípio de Pareto sugere que cerca de 80% dos erros são geralmente encontrados em 20% dos módulos do sistema. Isso significa que, se um defeito é encontrado em um módulo específico do software, as chances de haver outros defeitos nesse mesmo módulo são grandes. Portanto, faz sentido focar os esforços de teste nesses módulos para identificar e corrigir a maioria dos defeitos.

### 2º Objetivos do teste

Os testes de software têm como objetivo reduzir a probabilidade de erros no cliente, minimizar riscos ao negócio do cliente, atender às necessidades do cliente e, consequentemente, aumentar a satisfação do cliente. É importante entender os conceitos de erro, defeito e falha no contexto de testes de software. Um erro é uma ação humana que produz um resultado incorreto, um defeito é um estado do software causado por um erro, e uma falha é a manifestação de um defeito durante a execução do software.

Os defeitos podem ser originados de vários fatores, como pressão dos interessados no software, prazos inadequados, uso de tecnologia inadequada, falta de habilidade da equipe, não entendimento das necessidades do cliente e fator humano. Os defeitos ocorrem porque os softwares são escritos por pessoas, que, embora tenham habilidades, não são perfeitas e são suscetíveis a cometer erros. Além disso, os softwares são desenvolvidos sob pressão para serem entregues em prazos rigorosos, muitas vezes sem tempo para verificar as atividades realizadas.

A realização de testes é essencial, pois traz benefícios significativos, como a redução do índice de retrabalho na correção de falhas em produção e do tempo de homologação de uma nova versão. Além disso, aumenta o índice de falhas detectadas antes da produção, onde o custo de correção é mais baixo, e aumenta a abrangência dos testes.

Em resumo, os maiores benefícios dos testes de software são a motivação por maior segurança aos clientes, a oferta de maior continuidade do serviço ao negócio do cliente, a melhoria da qualidade dos softwares, a busca pela confiabilidade do software junto aos clientes e a redução de gastos em correção de bugs.

###  3º Mais sobre a Psicologia do Teste

A psicologia do teste de software destaca que a mentalidade usada durante os testes é diferente daquela usada durante a análise e o desenvolvimento. A independência no teste, evitando a influência do autor, é muitas vezes eficiente para encontrar defeitos e falhas. Existem diferentes níveis de independência, desde o teste elaborado pelo próprio autor do software até o teste elaborado por pessoas de diferentes organizações ou empresas.

As pessoas e os projetos são direcionados por objetivos. É importante ter objetivos claros para o teste. A identificação de falhas durante o teste pode ser vista como uma crítica ao produto e ao autor, mas é construtiva para o gerenciamento do risco do produto. A comunicação eficiente e a experiência para encontrar erros são essenciais.

O testador e o líder da equipe de teste precisam ter uma boa relação com as pessoas para comunicar informações sólidas sobre os defeitos, progresso e riscos de uma forma construtiva. Problemas de comunicação podem ocorrer, especialmente se os testadores forem vistos apenas como mensageiros de más notícias ao informar os defeitos. No entanto, existem maneiras de melhorar a comunicação e o relacionamento entre os testadores e os demais.

###  4º O processo de teste

O processo de teste de software segue o ciclo PDCA (Planejar, Fazer, Checar, Agir), que envolve a definição de metas e métodos, educação e treinamento, execução, coleta de dados, checagem e ações corretivas, preventivas e de melhoria. Um processo de teste básico deve incluir planejamento e controle, análise e modelagem, implementação e execução, avaliação de critérios de saída e relatórios, atividade de encerramento dos testes e ações de melhoria/corretiva/preventiva.

A Validação e a Verificação são conceitos fundamentais para a disciplina de teste. A Validação é a avaliação da veracidade do produto com base nas necessidades e requisitos definidos pelo cliente, enquanto a Verificação é a avaliação se o objeto foi desenvolvido da maneira prevista. A estrutura do modelo V é uma aproximação do processo de testes que pode ser integrada com todo o processo de desenvolvimento, focando em testar durante todo o ciclo de desenvolvimento para conseguir uma detecção adiantada dos erros.

### 5º Mais sobre os testes

O teste unitário visa testar a menor parte testável do sistema, como um módulo, objeto ou classe, de forma independente. Quando há dependência de métodos ou classes que não estão sob teste, são usados mocks para simular esses componentes. O Test Driven Development (TDD) é uma técnica onde os testes são escritos antes do código, permitindo uma abordagem gradual e a refatoração.

Os testes unitários podem substituir a função de um debugger, ajudando a identificar onde exatamente está o problema. Eles também são importantes para a manutenção posterior do código, pois todas as funcionalidades do sistema estão sendo testadas, reduzindo a possibilidade de bugs novos e inesperados. Além do teste unitário, existem outros níveis de teste, como integração, sistema e aceite, cada um com seus próprios objetivos e validações.

Os testes funcionais avaliam o funcionamento da aplicação, considerando o comportamento externo do software. Além dos testes funcionais, existem testes não funcionais que validam aspectos como desempenho e usabilidade, testes estruturais que focam na cobertura de código e testes de mudanças que validam a remoção de defeitos e a não geração de novos defeitos após alterações. As técnicas de teste podem ser classificadas em Caixa-preta, que derivam condições de teste de documentações formais e não consideram aspectos internos do código, e Caixa-branca, que consideram aspectos internos do software e focam nas estruturas dos códigos.


### Próximos Passos:

- Continuar aprofundando os conhecimentos em teste de software, explorando mais sobre Test Driven Development (TDD), testes de integração e técnicas de Caixa-preta e Caixa-branca;

- Aplicar os princípios e práticas aprendidos sobre teste de software em projetos reais, buscando oportunidades para aprimorar as habilidades de escrita de testes e colaboração com a equipe de desenvolvimento.







