## Resumo do 7º Dia de Estágio

## Data: 26/02/2024

## Resumo:

No Sétimo dia, estudamos sobre Banco de dados SQL e também fizemos um exercício prático. 

### 1º O que é SQL?

- **SQL** é uma linguagem de consulta usada para gerenciar e manipular bancos de dados relacionais.

- O SQL permite criar, recuperar, atualizar e excluir dados em tabelas.

Nos anos 1960, os primeiros bancos de dados computacionais surgiram. Nessa época, muitos cientistas da computação estavam focados em melhorar o funcionamento dos bancos de dados.
Um desses cientistas foi Edgar Frank (Ted) Codd, um britânico empregado pela IBM. Em 1970, ele publicou um artigo chamado “A Relational Model of Data for Large Shared Data Banks” (Um Modelo Relacional de Dados para Bancos de Dados Compartilhados Grandes), que marcou o início da era dos bancos de dados relacionais na ciência da computação.
Codd é frequentemente chamado de pai do SQL. Suas ideias revolucionaram a maneira como trabalhamos com bancos de dados.

### 2º Principais Comandos SQL:

- **SELECT**: Recupera dados de uma tabela.

    ```sql
     SELECT nome, idade FROM clientes;
     ```


- **INSERT**: Insere novos registros em uma tabela.

     ```sql
     INSERT INTO pedidos (numero, cliente_id, total) VALUES (1001, 3, 150.00);
     ```

- **UPDATE**: Atualiza valores existentes em uma tabela.

     ```sql
     UPDATE produtos SET preco = 25.99 WHERE categoria = 'Eletrônicos';
     ```

- **DELETE**: Remove registros de uma tabela.

     ```sql
     DELETE FROM clientes WHERE idade < 18;
     ```

### 3º Claúsulas Importantes:

- **WHERE**: Filtra resultados com base em condições específicas.

- **ORDER BY**: Classifica os resultados em ordem ascendente ou descendente.

- **GROUP BY**: Agrupa dados com base em colunas específicas.

- **JOIN**: Combina dados de várias tabelas.

### 4º Resolução Exercícios de SQL

- Com base na tabela de Usuários, você deve:


   **1- Realizar uma consulta que conte o número de registros na tabela.**

  ![Imagem de resolução exercício 1](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio%201.png)


   **2- Realizar uma consulta para encontrar o usuário com o id 10.**

  ![Imagem de resolução exercício 2](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio%202.png)



   **3- Realizar uma consulta para encontrar o usuário com o nome "Bruce Wayne".**

  ![Imagem de resolução exercício 3](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio%203.png)



   **4- Realizar uma consulta para encontrar o usuário com o e-mail "ghost_silva@fantasma.com".** 

  ![Imagem de resolução exercício 4](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio%204.png)



   **5- Realizar uma consulta para deletar o usuário com e-mail "peterparker@marvel.com".**

  ![Imagem de resolução exercício 5](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio%205.png)



- Com base na tabela de Produtos, você deve: 


   **6- Realizar uma consulta que apresente produtos com descrição vazia;**

  ![Imagem de resolução exercício 6](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio6.png)



   **7- Realizar uma consulta que apresente produtos com a categoria "games";**

  ![Imagem de resolução exercício 7](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio7.png)



   **8- Realizar uma consulta que apresente produtos com preço "0";**

  ![Imagem de resolução exercício 8](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio8.png)



   **9- Realizar uma consulta que apresente produtos com o preço maior que "100.00";**
  
  ![Imagem de resolução exercício 9](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio9.png)



   **10- Realizar uma consulta que apresente produtos com o preço entre "1000.00" e "2000.00";**

  ![Imagem de resolução exercício 10](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio10.png)



   **11- Realizar uma consulta que apresente produtos em que o nome contenha a palavra "jogo";**

  ![Imagem de resolução exercício 11](/sprint%201/Dia%207/Imagens%20resolução%20exercicios%20SQL/Exercicio11.png)
   


### Próximos Passos:

- Praticar Escrevendo consultas SQL em um ambiente de teste.

- Explorar tópicos avançados, como subconsultas e índices.