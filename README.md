# Programa De Bolsas Compass UOL

## Nome
**Programa De Bolsas Compass UOL - QA AWS - Trilha JavaRestassured**

## Descrição
Este repositório do programa de Bolsas está organizado por Sprints e por dia com o resumo dos pricipais aprendizados, seguindo a metodologia Ágil.

## Autores e agradecimentos
"Gostaria de estar agradecendo pela oportunidade de estar participando do Programa de Bolsas da Compass Uol em QA AWS - Trilha JavaRestassured onde estou tendo a oportunidade de estar evoluindo a cada dia e adquirindo conhecimentos que vou levar ao longo da minha carreira profissional."

- **Vinicius Marchesi**

## Status do Programa de Bolsas
Em andamento.
